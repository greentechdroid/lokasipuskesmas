package com.example.jonathansihombing.lokasipuskesmas.util;

import android.content.Intent;
import android.net.Uri;

import java.net.URL;

public class DirectionUtil {
    public static Intent startDirection(double destLatitude, double destLongitude) {
        String url = "http://maps.google.com/maps?daddr=" + destLatitude + "," + destLongitude;
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        //activity.startActivity(intent);
    }
}
