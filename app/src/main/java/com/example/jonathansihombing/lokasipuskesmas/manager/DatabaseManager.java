package com.example.jonathansihombing.lokasipuskesmas.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.jonathansihombing.lokasipuskesmas.MyApplication;

public class DatabaseManager extends SQLiteOpenHelper {

    private static DatabaseManager instance;

    public DatabaseManager(Context context) {
        super(context, MyApplication.DATABASE_NAME, null, MyApplication.DATABASE_VERSION);
    }

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        PuskesmasManager.buatTabel(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}