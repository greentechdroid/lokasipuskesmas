package com.example.jonathansihombing.lokasipuskesmas.controller;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;

import com.example.jonathansihombing.lokasipuskesmas.MyApplication;
import com.example.jonathansihombing.lokasipuskesmas.callback.PuskesmasCallback;
import com.example.jonathansihombing.lokasipuskesmas.manager.PuskesmasManager;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;
import com.example.jonathansihombing.lokasipuskesmas.util.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PuskesmasController {
    private Context context;
    private PuskesmasManager manager;
    private PuskesmasCallback callback;

    public PuskesmasController(PuskesmasCallback callback) {
        this.callback = callback;
        context = MyApplication.getContext();
        manager = PuskesmasManager.getInstance();
    }

    public void getPuskesmas() {
        try {
            callback.onGetPuskesmasSuccess(manager.tampildata());
        } catch (Exception e) {
            callback.onGetPuskesmasFailed(e);
            e.printStackTrace();
        }
    }

    public void getDataPuskesmas() {
        try {
            ArrayList<Puskesmas> list = new ArrayList<>();
            JSONObject obj = new JSONObject(JSONUtil.loadLocationJson(context, "puskesmas.json"));
            JSONArray data = obj.getJSONArray("data");
            int count = data.length();
            for (int i = 0; i < count; i++) {
                list.add(Puskesmas.parse(data.getJSONObject(i)));

            }
            callback.onGetPuskesmasSuccess(list);
            Log.d(".getData()", "Success get data gejala");

        } catch (JSONException ex) {
            callback.onGetPuskesmasFailed(ex);
            Log.d(".getData()", "failed get data gejala");
            ex.printStackTrace();
        }
    }
}
