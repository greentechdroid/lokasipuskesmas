package com.example.jonathansihombing.lokasipuskesmas.manager;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jonathansihombing.lokasipuskesmas.MyApplication;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;

import java.util.ArrayList;

public class PuskesmasManager {
    private static final String TAG = PuskesmasManager.class.getSimpleName();
    private static final String NAMA_TABEL = "Puskesmas";
    private static final String ID = "id";
    private static final String NAMA = "nama";
    private static final String ALAMAT = "alamat";
    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";
    private static final String KETERANGAN = "keterangan";
    private static final String TELP = "telp";
    private static final String EMAIL = "email";

    private static PuskesmasManager instance;


    public static PuskesmasManager getInstance() {
        if (instance == null) {
            instance = new PuskesmasManager();
        }
        return instance;
    }

    public static void buatTabel(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + NAMA_TABEL + "( "
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + NAMA + " TEXT,"
                + ALAMAT + " TEXT,"
                + LONGITUDE + " TEXT,"
                + LATITUDE + " TEXT,"
                + KETERANGAN + " TEXT,"
                + TELP + " TEXT,"
                + EMAIL + " TEXT"
                + ");");


    }

    private static Puskesmas parse(Cursor cursor) {
        Puskesmas model = null;
        try {
            model = new Puskesmas();
            model.setId(cursor.getInt(0));
            model.setNama(cursor.getString(1));
            model.setAlamat(cursor.getString(2));
            model.setLongitude(cursor.getDouble(3));
            model.setLatitude(cursor.getDouble(4));
            model.setKeterangan(cursor.getString(5));
            model.setTelp(cursor.getString(6));
            model.setEmail(cursor.getString(7));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    public long save(Puskesmas model) {
        SQLiteDatabase db = null;
        ContentValues cv = null;
        long count = 0;
        try {
            db = MyApplication.database.getWritableDatabase();
            cv = new ContentValues();
            cv.put(NAMA, model.getNama());
            cv.put(ALAMAT, model.getAlamat());
            cv.put(LONGITUDE, model.getLongitude());
            cv.put(LATITUDE, model.getLatitude());
            cv.put(KETERANGAN, model.getAlamat());
            cv.put(TELP, model.getTelp());
            cv.put(EMAIL, model.getEmail());


            db.beginTransaction();
            count = db.insert(NAMA_TABEL, null, cv);
            db.setTransactionSuccessful();
            Log.d(TAG + " save()", "Simpan Sukses");
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG + " save()", "Simpan Gagal");
            return -1;
        } finally {
            db.close();
        }
    }

    public ArrayList<Puskesmas> tampildata() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        ArrayList<Puskesmas> list;
        try {
            db = MyApplication.database.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM " + NAMA_TABEL + " ORDER BY " + ID + " ASC", null);
            list = new ArrayList<>();
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    list.add(parse(cursor));
                }
            }
            Log.d(TAG, " TAMPIL DATA sukses");
            return list;
        } catch (Exception e) {
            Log.d(TAG, "TAMPIL DATA gagal");
            e.printStackTrace();
            return null;
        } finally {
            cursor.close();
            db.close();
        }
    }

    public Puskesmas findById(long id) {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = MyApplication.database.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM " + NAMA_TABEL + "WHERE id=" + id + ";", null);
            while (cursor.moveToNext()) {
                return parse(cursor);
            }
            return null;

        } catch (Exception e) {
            return null;
        } finally {
            cursor.close();
            db.close();
        }
    }

    public int update(Puskesmas model) {
        SQLiteDatabase db = null;
        ContentValues cv = null;
        int count = 0;
        try {
            cv.put(NAMA, model.getNama());
            cv.put(ALAMAT, model.getAlamat());
            cv.put(LONGITUDE, model.getLongitude());
            cv.put(LATITUDE, model.getLatitude());
            cv.put(KETERANGAN, model.getKeterangan());
            cv.put(TELP, model.getTelp());
            cv.put(EMAIL, model.getEmail());
            db.beginTransaction();
            count = db.update(NAMA_TABEL, cv, "id=?", new String[]{model.getId() + ""});
            db.setTransactionSuccessful();
            return count;
        } catch (Exception e) {
            Log.e(TAG, "gagal update data");
            e.printStackTrace();
            return -1;
        } finally {
            db.close();
        }
    }

    public Puskesmas delete(int id) {
        SQLiteDatabase db = null;
        Puskesmas model = null;
        try {
            model = findById(id);
            db = MyApplication.database.getWritableDatabase();
            db.delete(NAMA_TABEL, "id=?", new String[]{id + ""});
            Log.d(TAG + "delete()", "berhasil hapus data");
            return model;
        } catch (Exception e) {
            Log.e(TAG, "gagal hapus data");
            e.printStackTrace();
            return null;
        } finally {
            db.close();
        }
    }
}
