package com.example.jonathansihombing.lokasipuskesmas.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jonathansihombing.lokasipuskesmas.R;

public class LokasiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener   {

    public ImageView ivIcon;
    public TextView tvIcon;
    public TextView tvTitle;
    public TextView tvvAddress;
    public TextView tvInformation;

    private ItemClickListener listener;

    public LokasiViewHolder(View itemView) {
        super(itemView);
        ivIcon = itemView.findViewById(R.id.iv_icon_profile) ;
        tvIcon = itemView.findViewById(R.id.tv_icon_text);
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvvAddress = itemView.findViewById(R.id.tv_address);
        tvInformation = itemView.findViewById(R.id.tv_information);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) { listener.onItemClick(view, getAdapterPosition()); }

    public void setOnItemClickListener(ItemClickListener listener) { this.listener = listener; }

    public void clearAnimation() { itemView.clearAnimation() ; }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
