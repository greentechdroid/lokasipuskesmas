package com.example.jonathansihombing.lokasipuskesmas.callback;

import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;

import java.util.ArrayList;

public interface PuskesmasCallback {
    void onGetPuskesmasSuccess(ArrayList<Puskesmas> results);
    void onGetPuskesmasFailed(Throwable e);
}
