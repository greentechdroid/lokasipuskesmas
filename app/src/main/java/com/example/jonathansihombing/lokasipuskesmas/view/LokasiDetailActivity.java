package com.example.jonathansihombing.lokasipuskesmas.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;
import com.example.jonathansihombing.lokasipuskesmas.model.Temp;

import static com.example.jonathansihombing.lokasipuskesmas.util.DirectionUtil.startDirection;

public class LokasiDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private TextView tvAddress, tvLatitude, tvDescription, tvLongitude;
    private CollapsingToolbarLayout collapsingToolbar;
    private ImageView ivThumbnail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_lokasi_detail);
        initView();
        setupView();
    }

    @Override
    protected void onDestroy() {
        try {
            Temp.puskesmasModel = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void initView() {
        //WidgetUtil.setHeight(this, appBarLayout);
        toolbar = findViewById(R.id.toolbar);
        Button btnVisit = findViewById(R.id.btn_visit);
        btnVisit.setOnClickListener(this);
        tvAddress = findViewById(R.id.tv_address);
        tvLatitude = findViewById(R.id.tv_latitude);
        tvLongitude = findViewById(R.id.tv_longitude);
        tvDescription = findViewById(R.id.tv_information);
        ivThumbnail = findViewById(R.id.iv_thumbnail);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
    }

    private void setupView() {
        Puskesmas item = Temp.puskesmasModel;
        if (item != null) {
            tvAddress.setText(item.getAlamat());
            tvLatitude.setText(String.valueOf(item.getLatitude()));
            tvLongitude.setText(String.valueOf(item.getLongitude()));
            tvDescription.setText(item.getKeterangan());
            collapsingToolbar.setTitle(item.getNama());
            Glide.with(this).load(item.getImage()).into(ivThumbnail);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_visit:
                startActivity(startDirection(Temp.puskesmasModel.getLatitude(), Temp.puskesmasModel.getLongitude()));
                break;
        }
    }
}