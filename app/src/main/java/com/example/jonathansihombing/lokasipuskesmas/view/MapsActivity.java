package com.example.jonathansihombing.lokasipuskesmas.view;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.callback.PuskesmasCallback;
import com.example.jonathansihombing.lokasipuskesmas.controller.PuskesmasController;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, PuskesmasCallback {

    private GoogleMap mMap;
    private PuskesmasController controller;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        controller = new PuskesmasController(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        controller.getDataPuskesmas();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onGetPuskesmasSuccess(final ArrayList<Puskesmas> results) {
        //-6.240653, 106.877541
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                for (Puskesmas item : results) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLatitude(), item.getLongitude())).title(item.getNama()));
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-6.240653, 106.877541)));
            }
        });
    }

    @Override
    public void onGetPuskesmasFailed(Throwable e) {
        Toast.makeText(this, "Gagal mengambil data", Toast.LENGTH_SHORT).show();
    }
}
