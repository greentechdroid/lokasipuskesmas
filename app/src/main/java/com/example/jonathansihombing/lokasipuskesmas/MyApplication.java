package com.example.jonathansihombing.lokasipuskesmas;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.example.jonathansihombing.lokasipuskesmas.manager.DatabaseManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyApplication extends Application {
    public static final String TAG = MyApplication.class.getSimpleName();
    public static final String DATABASE_NAME = "base_location.db";
    public static final byte DATABASE_VERSION = 1;

    private static Context context;
    private static MyApplication instance;
    private static Handler handler = new Handler();

    public static final ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public static DatabaseManager database = null;

    public static Context getContext() {
        return MyApplication.context;
    }

    public static synchronized MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getBaseContext();
        service.execute(new Runnable() {
            @Override
            public void run() {
                setupDatabase();
            }
        });
    }

    private synchronized void setupDatabase() {
        try {
            MyApplication.database = DatabaseManager.getInstance(context);
            Log.d(TAG, "setupDB");
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            e = null;
        }
    }

    public void runOnUiThread(final Runnable runnable) {
        handler.post(runnable);
    }

    public void runOnUiThreadDelay(Runnable run, long timemilis) {
        handler.postDelayed(run, timemilis);
    }


    public void runInBackground(final Runnable runnable) {
        service.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } catch (Exception e) {
                    Log.e(TAG, "backgroundThread");
                    e.printStackTrace();
                }
            }
        });
    }
}
