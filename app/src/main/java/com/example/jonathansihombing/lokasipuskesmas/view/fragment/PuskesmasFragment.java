package com.example.jonathansihombing.lokasipuskesmas.view.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.jonathansihombing.lokasipuskesmas.MyApplication;
import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.callback.PuskesmasCallback;
import com.example.jonathansihombing.lokasipuskesmas.controller.PuskesmasController;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;
import com.example.jonathansihombing.lokasipuskesmas.model.Temp;
import com.example.jonathansihombing.lokasipuskesmas.view.LokasiDetailActivity;
import com.example.jonathansihombing.lokasipuskesmas.view.NewHomeActivity;
import com.example.jonathansihombing.lokasipuskesmas.view.adapter.LokasiAdapter;
import com.example.jonathansihombing.lokasipuskesmas.view.adapter.LokasiViewHolder;

import java.util.ArrayList;

public class PuskesmasFragment extends Fragment implements LokasiViewHolder.ItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, PuskesmasCallback {
    private PuskesmasController controller;
    private LokasiAdapter mAdapter;

    private RecyclerView recyclerView;
    private SearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_puskesmas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initInstance();
        setupRecyclerView();
        getData();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() == 0) {
                    onRefresh();

                } else {
                    mAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            return false;
        }
        return true;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        MyApplication.getInstance().runOnUiThreadDelay(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }, 1000);
    }

    @Override
    public void onGetPuskesmasSuccess(ArrayList<Puskesmas> results) {
        mAdapter.addItem(results);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetPuskesmasFailed(Throwable e) {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(View view, int position) {
        Temp.puskesmasModel = mAdapter.getItemPosition(position);
        startActivity(new Intent(getActivity(), LokasiDetailActivity.class));
    }

    private void getData() {
        try {
            controller.getDataPuskesmas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void initInstance() {
        controller = new PuskesmasController(this);
        mAdapter = new LokasiAdapter();
        mAdapter.setOnItemClickListener(this);
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }
}
