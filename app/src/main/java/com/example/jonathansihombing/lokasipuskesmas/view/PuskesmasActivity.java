package com.example.jonathansihombing.lokasipuskesmas.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.jonathansihombing.lokasipuskesmas.MyApplication;
import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.callback.PuskesmasCallback;
import com.example.jonathansihombing.lokasipuskesmas.controller.PuskesmasController;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;
import com.example.jonathansihombing.lokasipuskesmas.model.Temp;
import com.example.jonathansihombing.lokasipuskesmas.view.adapter.LokasiAdapter;
import com.example.jonathansihombing.lokasipuskesmas.view.adapter.LokasiViewHolder;

import java.util.ArrayList;

public class PuskesmasActivity extends AppCompatActivity implements LokasiViewHolder.ItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, PuskesmasCallback {

    private PuskesmasController controller;
    private LokasiAdapter mAdapter;

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puskesmas);
        initView();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Puskesmas");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initInstance();
        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() == 0) {
                    onRefresh();

                } else {
                    mAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            return true;

        } else {
            return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        mAdapter.clearAdapter();
        super.onDestroy();
    }

    @Override
    public void onItemClick(View view, int position) {
        Temp.puskesmasModel = mAdapter.getItemPosition(position);
        startActivity(new Intent(this, LokasiDetailActivity.class));
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        MyApplication.getInstance().runOnUiThreadDelay(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }, 1500);
    }

    private void getData() {
        try {
            controller.getDataPuskesmas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerview);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void initInstance() {
        controller = new PuskesmasController(this);
        mAdapter = new LokasiAdapter();
        mAdapter.setOnItemClickListener(this);
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onGetPuskesmasSuccess(ArrayList<Puskesmas> results) {
        mAdapter.addItem(results);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetPuskesmasFailed(Throwable e) {
        swipeRefreshLayout.setRefreshing(false);
    }
}
