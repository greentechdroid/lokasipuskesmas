package com.example.jonathansihombing.lokasipuskesmas.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.model.Puskesmas;

import java.util.ArrayList;

public class LokasiAdapter extends RecyclerView.Adapter<LokasiViewHolder> implements Filterable {

    private Context context;
    private ArrayList<Puskesmas> items;
    private ArrayList<Puskesmas> itemFilter;
    private LokasiViewHolder.ItemClickListener listener;

    public LokasiAdapter() {
        this.items = new ArrayList<>();
    }

    @NonNull
    @Override
    public LokasiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_lokasi_list, parent, false);
        LokasiViewHolder holder = new LokasiViewHolder(view);
        holder.setOnItemClickListener(clickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull LokasiViewHolder holder, int position) {
        Puskesmas item = itemFilter.get(position);
        if (item != null) {
            holder.tvTitle.setText(item.getNama());
            holder.tvvAddress.setText(item.getAlamat());
            if (item.getKeterangan() != null && !item.getKeterangan().isEmpty())
                holder.tvInformation.setText(item.getKeterangan());
            else
                holder.tvInformation.setVisibility(View.GONE);
            holder.tvIcon.setText(item.getNama().substring(0, 1));
        }
    }

    @Override
    public int getItemCount() {
        return itemFilter != null ? itemFilter.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                try {
                    String charString = charSequence.toString();
                    FilterResults filterResults = new FilterResults();
                    if (charString.isEmpty()) {
                        itemFilter = items;
                    } else {
                        ArrayList<Puskesmas> itemFiltered = new ArrayList<>();
                        for (Puskesmas item : items) {
                            if (item.getNama().toLowerCase().contains(charString.toLowerCase())) {
                                itemFiltered.add(item);
                            }
                        }
                        itemFilter = itemFiltered;
                        filterResults.values = itemFilter;
                    }
                    return filterResults;
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemFilter = (ArrayList<Puskesmas>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void addItem(ArrayList<Puskesmas> model) {
        if (model != null) {
            items.clear();
            items.addAll(model);
            itemFilter = items;
            notifyDataSetChanged();
        }
    }

    public Puskesmas getItemPosition(int position) {
        return items.get(position);
    }

    public void setOnItemClickListener(LokasiViewHolder.ItemClickListener listener) {
        this.listener = listener;
    }

    private LokasiViewHolder.ItemClickListener clickListener = new LokasiViewHolder.ItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            listener.onItemClick(view, position);
        }
    };

    public void clearAdapter() {
        try {
            context = null;
            items = null;
            itemFilter = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
