package com.example.jonathansihombing.lokasipuskesmas.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Puskesmas {

    /*Puskesmas = id, nama, longitude, latitude, keterangan, telp, email*/
    private int id;
    private String nama;
    private String alamat;
    private double longitude;
    private double latitude;
    private String keterangan;
    private String telp;
    private String email;
    private String image;

    public Puskesmas() {

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static Puskesmas parse(JSONObject object) throws JSONException {
        if (object == null) throw new JSONException("null object");
        Puskesmas puskesmas = new Puskesmas();
        puskesmas.setNama(object.getString("nama"));
        puskesmas.setAlamat(object.getString("alamat"));
        puskesmas.setLongitude(object.getDouble("longitude"));
        puskesmas.setLatitude(object.getDouble("latitude"));
        puskesmas.setKeterangan(object.getString("keterangan"));
        puskesmas.setTelp(object.getString("telp"));
        puskesmas.setEmail(object.getString("email"));
        puskesmas.setImage(object.getString("image"));
        return puskesmas;
    }
}
