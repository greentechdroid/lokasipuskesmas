package com.example.jonathansihombing.lokasipuskesmas.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.jonathansihombing.lokasipuskesmas.R;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button ivMap;
    private Button ivInfo;
    private Button ivDaftarPuskesmas;
    private Button ivExit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_home);
        ivMap = findViewById(R.id.iv_map);
        ivMap.setOnClickListener(this);
        ivInfo = findViewById(R.id.iv_info);
        ivInfo.setOnClickListener(this);
        ivDaftarPuskesmas = findViewById(R.id.iv_daftar_puskesmas);
        ivDaftarPuskesmas.setOnClickListener(this);
        ivExit = findViewById(R.id.iv_exit);
        ivExit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_map:
                startActivity(new Intent(this, MapsActivity.class));
                break;
            case R.id.iv_info:
                startActivity(WebViewActivity.createIntent(this, "Info Puskesmas", "http://satulayanan.id/layanan/index/54/puskesmas/kemenkes"));
                break;
            case R.id.iv_daftar_puskesmas:
                startActivity(new Intent(this, PuskesmasActivity.class));
                break;
            case R.id.iv_exit:
                finish();
                break;
        }
    }
}
