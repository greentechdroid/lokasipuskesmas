package com.example.jonathansihombing.lokasipuskesmas.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.jonathansihombing.lokasipuskesmas.R;
import com.example.jonathansihombing.lokasipuskesmas.util.WidgetUtil;
import com.example.jonathansihombing.lokasipuskesmas.view.fragment.MapFragment;
import com.example.jonathansihombing.lokasipuskesmas.view.fragment.PuskesmasFragment;
import com.example.jonathansihombing.lokasipuskesmas.view.fragment.WebViewFragment;

import java.util.List;

public class NewHomeActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private AppBarLayout appBarLayout;
    private Fragment currentFragment;
    private MapFragment mapsFragment;
    private PuskesmasFragment puskesmasFragment;
    private WebViewFragment webViewFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (currentFragment != mapsFragment) {
                        setCurrentFragment(mapsFragment);
                        setTitleToolbar("Map");
                    }
                    return true;
                case R.id.navigation_dashboard:
                    if (currentFragment != puskesmasFragment) {
                        setCurrentFragment(puskesmasFragment);
                        setTitleToolbar("Lokasi Detail");
                    }
                    return true;
                case R.id.navigation_notifications:
                    if (currentFragment != webViewFragment) {
                        setCurrentFragment(webViewFragment);
                        setTitleToolbar("Puskesmas");
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        appBarLayout = findViewById(R.id.appbar);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initFragment();
        setCurrentFragment(mapsFragment);
    }

    private void setCurrentFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        if (!fragment.isAdded()) {
            fragmentTransaction.add(R.id.frame_fragment, fragment);
        } else {
            fragmentTransaction.show(fragment);
        }

        @SuppressLint("RestrictedApi") List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                if (f != fragment) {
                    fragmentTransaction.hide(f);
                }
            }
        }

        fragmentTransaction.commitAllowingStateLoss();
    }

    private void initFragment() {
        if (mapsFragment == null) {
            mapsFragment = new MapFragment();
        }
        if (puskesmasFragment == null) {
            puskesmasFragment = new PuskesmasFragment();
        }
        if (webViewFragment == null) {
            webViewFragment = new WebViewFragment();
        }
    }

    private void setTitleToolbar(String title) {
        toolbar.setTitle(title);
    }

}
