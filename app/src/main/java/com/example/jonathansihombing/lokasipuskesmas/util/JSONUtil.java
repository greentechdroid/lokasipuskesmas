package com.example.jonathansihombing.lokasipuskesmas.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

public class JSONUtil {
    public static String loadLocationJson(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return json;

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
